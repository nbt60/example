package main

import (
	"gitlab.com/empowerlab/example/service-product/graphql"
	"gitlab.com/empowerlab/example/service-product/grpc"
	"gitlab.com/empowerlab/example/service-product/orm"
)

func main() {
	orm.Definitions.GenProtos()
	orm.Definitions.Gen()
	grpc.Definitions.GenProtos()
	grpc.Definitions.GenServer()
	grpc.Definitions.GenClient()
	graphql.Definitions.Gen()
}
