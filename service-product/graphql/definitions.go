package graphql

import (
	"gitlab.com/empowerlab/stack/lib-go/libgraphql"
	// "gitlab.com/empowerlab/stack/lib-go/libgrpc"
	// "gitlab.com/empowerlab/stack/lib-go/libmodel"

	"gitlab.com/empowerlab/example/service-product/grpc"
)

var Definitions *libgraphql.Definitions

func init() {

	Definitions = &libgraphql.Definitions{
		Name:       "product",
		Repository: "gitlab.com/empowerlab/example/service-product",
	}

	Definitions.Register(&libgraphql.Definition{
		Grpc: grpc.Definitions.GetByID("product"),
	})

}
