FROM golang:1.13-alpine AS build

# WORKDIR /go/src/app
# COPY . .

RUN apk add git make protobuf

RUN wget https://github.com/grpc-ecosystem/grpc-health-probe/releases/download/v0.3.0/grpc_health_probe-linux-amd64 -O /usr/bin/grpc_health_probe
RUN chmod +x /usr/bin/grpc_health_probe

# RUN mkdir -p $GOPATH/src/gitlab.com/empowerlab/example
COPY . /go/src/gitlab.com/empowerlab/example
# RUN cp -R ./* $GOPATH/src/gitlab.com/empowerlab/example

RUN go get github.com/gogo/protobuf/protoc-gen-gogofaster
RUN go install github.com/gogo/protobuf/protoc-gen-gogofaster
RUN cd /go/src/gitlab.com/empowerlab/example/gateway  && make dep || true
RUN cd /go/src/gitlab.com/empowerlab/stack/login && make dep || true
RUN cd /go/src/gitlab.com/empowerlab/stack/login && go generate

RUN cd /go/src/gitlab.com/empowerlab/example/service-customer && make dep || true
RUN cd /go/src/gitlab.com/empowerlab/example/service-customer  && go generate
RUN cd /go/src/gitlab.com/empowerlab/example/service-customer && make dep
RUN cd /go/src/gitlab.com/empowerlab/example/service-customer && make install

RUN cd /go/src/gitlab.com/empowerlab/example/service-product && make dep || true
RUN cd /go/src/gitlab.com/empowerlab/example/service-product  && go generate
RUN cd /go/src/gitlab.com/empowerlab/example/service-product && make dep
RUN cd /go/src/gitlab.com/empowerlab/example/service-product && make install

RUN cd /go/src/gitlab.com/empowerlab/example/service-invoicing && make dep || true
RUN cd /go/src/gitlab.com/empowerlab/example/service-invoicing  && go generate
RUN cd /go/src/gitlab.com/empowerlab/example/service-invoicing && make dep
RUN cd /go/src/gitlab.com/empowerlab/example/service-invoicing && make install

RUN cd /go/src/gitlab.com/empowerlab/example/gateway  && make dep
RUN cd /go/src/gitlab.com/empowerlab/example/gateway && make install


FROM alpine

COPY --from=build /usr/bin/grpc_health_probe /usr/bin/grpc_health_probe

ENV GOPATH /go
ENV PATH /go/bin:$PATH

COPY --from=build /go/src/gitlab.com/empowerlab/example/service-customer /go/src/gitlab.com/empowerlab/example/service-customer
COPY --from=build /go/src/gitlab.com/empowerlab/example/service-product /go/src/gitlab.com/empowerlab/example/service-product
COPY --from=build /go/src/gitlab.com/empowerlab/example/service-invoicing /go/src/gitlab.com/empowerlab/example/service-invoicing
COPY --from=build /go/src/gitlab.com/empowerlab/example/gateway /go/src/gitlab.com/empowerlab/example/gateway
COPY --from=build /go/bin/service-customer /go/bin/service-customer
COPY --from=build /go/bin/service-product /go/bin/service-product
COPY --from=build /go/bin/service-invoicing /go/bin/service-invoicing
COPY --from=build /go/bin/gateway /go/bin/gateway

