package orm

import (
	"fmt"
	"io"
	"os"

	"github.com/joho/godotenv"
	"gitlab.com/empowerlab/stack/lib-go/libdata"
	"gitlab.com/empowerlab/stack/lib-go/libdata/drivers/grpc"
	"gitlab.com/empowerlab/stack/lib-go/libdata/fields"
	"gitlab.com/empowerlab/stack/lib-go/libdata/specials"
	// "gitlab.com/empowerlab/stack/lib-go/libgrpc"
	opentracing "github.com/opentracing/opentracing-go"
	jaeger "github.com/uber/jaeger-client-go"
	config "github.com/uber/jaeger-client-go/config"
	"gitlab.com/empowerlab/stack/lib-go/liborm"

	customer "gitlab.com/empowerlab/example/service-customer/gen/grpc/client"
	product "gitlab.com/empowerlab/example/service-product/gen/grpc/client"
)

var Definitions *liborm.Definitions

var CustomerClient *customer.Client
var ProductClient *product.Client

var Tracer opentracing.Tracer
var TracerCloser io.Closer

func init() {

	err := godotenv.Load()
	if err != nil {
		fmt.Println("Error loading .env file")
	}

	cfg := &config.Configuration{
		Sampler: &config.SamplerConfig{
			Type:  "const",
			Param: 1,
		},
		Reporter: &config.ReporterConfig{
			LogSpans:          false,
			CollectorEndpoint: os.Getenv("JAEGER_ENDPOINT"),
		},
	}
	Tracer, TracerCloser, err = cfg.New("service-invoicing", config.Logger(jaeger.StdLogger))
	if err != nil {
		panic(fmt.Sprintf("ERROR: cannot init Jaeger: %v\n", err))
	}

	CustomerClient, err = customer.NewClient(os.Getenv("CUSTOMER_SERVICE_HOST"), Tracer)
	if err != nil {
		fmt.Println("Impossible to connect to customer service")
	}
	fmt.Printf("customerClient %+v\n", CustomerClient)

	ProductClient, err = product.NewClient(os.Getenv("PRODUCT_SERVICE_HOST"), Tracer)
	if err != nil {
		fmt.Println("Impossible to connect to product service")
	}
	fmt.Printf("productClient %+v\n", ProductClient)

	Definitions = &liborm.Definitions{
		Repository: "gitlab.com/empowerlab/example/service-invoicing",
	}

	customerDriver := &grpc.Driver{
		Client:     CustomerClient,
		Import:     "gitlab.com/empowerlab/example/service-customer/gen/orm",
		ImportName: "customermodels",
	}

	productDriver := &grpc.Driver{
		Client:     ProductClient,
		Import:     "gitlab.com/empowerlab/example/service-product/gen/orm",
		ImportName: "productmodels",
	}

	Definitions.Register(&liborm.Definition{
		Model: &libdata.ModelDefinition{
			Name: "customer",
			Fields: []libdata.Field{
				&fields.Text{Name: "name", Required: true},
				&fields.Text{Name: "street"},
				&fields.Text{Name: "zip"},
				&fields.Text{Name: "city"},
				&fields.Text{Name: "country"},
			},
			Datetime:             true,
			CanAssignID:          false,
			DisableDatabaseStore: true,
			ExternalDriver:       customerDriver,
		},
	})

	Definitions.Register(&liborm.Definition{
		Model: &libdata.ModelDefinition{
			Name: "product",
			Fields: []libdata.Field{
				&fields.Text{Name: "name", Required: true, Store: false},
				&fields.Float{Name: "price", Required: true},
			},
			Datetime:       true,
			CanAssignID:    true,
			ExternalDriver: productDriver,
		},
	})

	Definitions.Register(&liborm.Definition{
		Model: &libdata.ModelDefinition{
			Name: "invoice",
			Fields: []libdata.Field{
				&fields.Text{Name: "number", Required: true},
				&fields.Many2one{Name: "customerUUID", Reference: "customer", Required: true},
				&fields.Text{Name: "state", Required: true},
				&fields.One2many{Name: "lines", Reference: "invoiceLine", InverseField: "invoiceUUID"},
			},
			Datetime:    true,
			CanAssignID: false,
		},
	})

	Definitions.Register(&liborm.Definition{
		Model: &libdata.ModelDefinition{
			Name: "invoiceLine",
			Fields: []libdata.Field{
				&fields.Many2one{Name: "invoiceUUID", Reference: "invoice", Required: true},
				&fields.Many2one{Name: "productUUID", Reference: "product", Required: true},
				&fields.Text{Name: "name", Required: true},
				&fields.Integer{Name: "quantity", Required: true},
				&fields.Float{Name: "priceUnit", Required: true},
			},
			Datetime:    true,
			CanAssignID: false,
		},
	})

	Definitions.Register(&liborm.Definition{
		Model: specials.EventModel,
	})
}
