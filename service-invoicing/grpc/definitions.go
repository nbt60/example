package grpc

import (
	"gitlab.com/empowerlab/stack/lib-go/libgrpc"

	"gitlab.com/empowerlab/example/service-invoicing/orm"
)

var Definitions *libgrpc.Definitions

func init() {

	Definitions = &libgrpc.Definitions{
		Prefix:     "invoicing",
		Repository: "gitlab.com/empowerlab/example/service-invoicing",
	}

	Definitions.Register(&libgrpc.Definition{
		ORM: orm.Definitions.GetByID("customer"),
	})

	Definitions.Register(&libgrpc.Definition{
		ORM: orm.Definitions.GetByID("product"),
	})

	Definitions.Register(&libgrpc.Definition{
		ORM: orm.Definitions.GetByID("invoice"),
	})

	Definitions.Register(&libgrpc.Definition{
		ORM: orm.Definitions.GetByID("invoiceLine"),
	})

	// dataModel := models.Definitions.GetByID("data")
	// Definitions.Register(&libgrpc.Definition{
	// 	Model:       dataModel,
	// 	CustomFuncs: dataModel.CustomFuncsCollection,
	// })

}
