package main

import (
	"gitlab.com/empowerlab/example/service-invoicing/graphql"
	"gitlab.com/empowerlab/example/service-invoicing/grpc"
	"gitlab.com/empowerlab/example/service-invoicing/orm"
)

func main() {
	orm.Definitions.GenProtos()
	orm.Definitions.Gen()
	grpc.Definitions.GenProtos()
	grpc.Definitions.GenServer()
	grpc.Definitions.GenClient()
	graphql.Definitions.Gen()
}
