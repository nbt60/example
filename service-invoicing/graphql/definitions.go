package graphql

import (
	"gitlab.com/empowerlab/stack/lib-go/libgraphql"
	// "gitlab.com/empowerlab/stack/lib-go/libgrpc"
	// "gitlab.com/empowerlab/stack/lib-go/libmodel"

	"gitlab.com/empowerlab/example/service-invoicing/grpc"
)

var Definitions *libgraphql.Definitions

func init() {

	Definitions = &libgraphql.Definitions{
		Name:       "invoicing",
		Repository: "gitlab.com/empowerlab/example/service-invoicing",
	}

	Definitions.Register(&libgraphql.Definition{
		Grpc:          grpc.Definitions.GetByID("customer"),
		DisableSelect: true,
		DisableCreate: true,
		DisableUpdate: true,
		DisableDelete: true,
	})

	Definitions.Register(&libgraphql.Definition{
		Grpc:          grpc.Definitions.GetByID("product"),
		DisableSelect: true,
		DisableCreate: true,
		DisableUpdate: true,
		DisableDelete: true,
	})

	Definitions.Register(&libgraphql.Definition{
		Grpc: grpc.Definitions.GetByID("invoice"),
	})

	Definitions.Register(&libgraphql.Definition{
		Grpc: grpc.Definitions.GetByID("invoiceLine"),
	})

	// dataGRPC := grpc.Definitions.GetByID("data")
	// Definitions.Register(&libgraphql.Definition{
	// 	Grpc: grpc.Definitions.GetByID("data"),
	// 	CustomFuncs: []*libmodel.CustomFunc{
	// 		dataGRPC.GetCustomFuncByName("loadData"),
	// 	},
	// })

}
