package main

import (
	// "bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"

	// "github.com/go-openapi/runtime"
	graphqlServer "github.com/graph-gophers/graphql-go"
	"github.com/graph-gophers/graphql-go/relay"
	"github.com/joho/godotenv"
	hydraclient "github.com/ory/hydra/sdk/go/hydra/client"
	hydra "github.com/ory/hydra/sdk/go/hydra/client/admin"
	"github.com/pkg/errors"
	"github.com/rs/cors"
	"gitlab.com/empowerlab/stack/lib-go/lib"
	"gitlab.com/empowerlab/stack/lib-go/libgraphql"
	"golang.org/x/net/context"

	"gitlab.com/empowerlab/example/gateway/graphql"
	customerGraphql "gitlab.com/empowerlab/example/service-customer/graphql"
	invoicingGraphql "gitlab.com/empowerlab/example/service-invoicing/graphql"
	productGraphql "gitlab.com/empowerlab/example/service-product/graphql"
	loginclient "gitlab.com/empowerlab/stack/login/gen/graphql"
	loginGraphql "gitlab.com/empowerlab/stack/login/graphql"
)

var schema *graphqlServer.Schema
var hclient *hydra.Client

func init() {

	log.Println("Starting...")

	// nolint: errcheck, gas
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	for _, e := range os.Environ() {
		fmt.Println(e)
	}

	adminURL, err := url.Parse(os.Getenv("HYDRA_CLUSTER_URL"))
	if err != nil {
		log.Fatal(err)
	}
	oryhydra := hydraclient.NewHTTPClientWithConfig(nil, &hydraclient.TransportConfig{Schemes: []string{adminURL.Scheme}, Host: adminURL.Host, BasePath: adminURL.Path})
	hclient = oryhydra.Admin

	schema = graphqlServer.MustParseSchema(
		libgraphql.GenSchema([]*libgraphql.SchemaData{
			{
				Name:        "login",
				Definitions: loginGraphql.Definitions,
			},
			{
				Name:        "customer",
				Definitions: customerGraphql.Definitions,
			},
			{
				Name:        "product",
				Definitions: productGraphql.Definitions,
			},
			{
				Name:        "invoicing",
				Definitions: invoicingGraphql.Definitions,
			},
		}),
		&graphql.Resolver{})

	log.Println("Schema loaded...")

	log.Println("Init done...")
}

func auth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		fmt.Println("header ", r.Header)
		token := r.Header.Get("Authorization")
		token = strings.Replace(token, "Bearer ", "", 1)

		// jsonData := map[string]string{"token": token, "scope": "openid"}
		// jsonValue, _ := json.Marshal(jsonData)

		fmt.Println("token", token)
		if token != "" {

			// headers := map[string][]string{
			// 	"Content-Type": []string{"application/x-www-form-urlencoded"},
			// 	"Accept":       []string{"application/json"},
			// }

			body := url.Values{"token": {token}, "scope": {"openid"}}
			response, err := http.Post(os.Getenv("HYDRA_CLUSTER_URL")+"/oauth2/introspect", "application/x-www-form-urlencoded", strings.NewReader(body.Encode())) //bytes.NewBuffer(jsonValue))
			if err != nil {
				fmt.Println(err)
				http.Error(w, errors.Wrap(err, "The accept login request endpoint encountered a network error").Error(), http.StatusInternalServerError)
			}

			dataByte, _ := ioutil.ReadAll(response.Body)
			data := map[string]string{}
			_ = json.Unmarshal(dataByte, &data)
			fmt.Println(data)

			roles, err := loginclient.GrpcClient.GetRolesByAccount(r.Context(), data["sub"])
			if err != nil {
				http.Error(w, errors.Wrap(err, "Couldn't get groups").Error(), http.StatusInternalServerError)
			}
			var rolesSlice []string
			for _, role := range roles {
				rolesSlice = append(rolesSlice, role.Name)
			}

			ctx = context.WithValue(ctx, "token", strings.Replace(token, "Bearer ", "", 1))
			ctx = context.WithValue(ctx, "userUUID", data["sub"])
			ctx = context.WithValue(ctx, "groups", strings.Join(rolesSlice, ","))
		}

		//nolint: golint
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func main() {

	// defer graphql.Closer.Close()

	http.Handle("/", lib.GraphiQL{})
	http.Handle("/graphql", cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedHeaders:   []string{"Authorization", "Content-Type"},
		AllowCredentials: true,
		Debug:            true,
	}).Handler(auth(&relay.Handler{Schema: schema})))

	log.Println("Listening on port ", os.Getenv("GATEWAY_AUTO_DEPLOY_SERVICE_PORT"), "...")
	err := http.ListenAndServe(fmt.Sprintf(":%s", os.Getenv("GATEWAY_AUTO_DEPLOY_SERVICE_PORT")), nil)
	if err != nil {
		panic(fmt.Sprintf("Couldn't start server %s", err))
	}

}

// Tests inspiration: https://medium.com/free-code-camp/how-to-write-bulletproof-code-in-go-a-workflow-for-servers-that-cant-fail-10a14a765f22
