package main

import (
	"gitlab.com/empowerlab/example/service-customer/graphql"
	"gitlab.com/empowerlab/example/service-customer/grpc"
	"gitlab.com/empowerlab/example/service-customer/orm"
)

func main() {
	orm.Definitions.GenProtos()
	orm.Definitions.Gen()
	grpc.Definitions.GenProtos()
	grpc.Definitions.GenServer()
	grpc.Definitions.GenClient()
	graphql.Definitions.Gen()
}
