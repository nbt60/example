package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/joho/godotenv"
	nats "github.com/nats-io/nats.go"
	opentracing "github.com/opentracing/opentracing-go"
	"github.com/pkg/errors"
	jaeger "github.com/uber/jaeger-client-go"
	config "github.com/uber/jaeger-client-go/config"
	"gitlab.com/empowerlab/stack/lib-go/libdata"
	"gitlab.com/empowerlab/stack/lib-go/libdata/drivers/memory"
	"gitlab.com/empowerlab/stack/lib-go/libdata/drivers/postgres"
	"gitlab.com/empowerlab/stack/lib-go/libgrpc"
	"gitlab.com/empowerlab/stack/lib-go/liborm"

	_ "gitlab.com/empowerlab/example/service-customer/gen/grpc/server"
	gengrpc "gitlab.com/empowerlab/example/service-customer/gen/grpc/server"
	genmodels "gitlab.com/empowerlab/example/service-customer/gen/orm"
)

//nolint: lll
//go:generate go run gen/gen.go
//go:generate protoc -I=. -I=$GOPATH/src -I=$GOPATH/src/github.com/gogo/protobuf/protobuf --proto_path=./gen/orm/pb --gogofaster_out=plugins=grpc:./gen/orm/pb main.proto
//go:generate protoc -I=. -I=$GOPATH/src -I=$GOPATH/src/github.com/gogo/protobuf/protobuf --proto_path=./gen/grpc/pb --gogofaster_out=plugins=grpc:./gen/grpc/pb main.proto

func init() {

	log.Println("Starting...")

	// nolint: errcheck, gas
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	// for _, e := range os.Environ() {
	// 	fmt.Println(e)
	// }

	fmt.Println(nats.DefaultURL)
	fmt.Println(os.Getenv("NATS_HOST"))
	libdata.NatsClient, err = nats.Connect(os.Getenv("NATS_HOST"))
	if err != nil {
		fmt.Printf("Impossible to connect to NATS: %v\n", err)
	}

	fmt.Println(os.Getenv("DATABASE_URL"))
	postgresDriver := &postgres.Driver{}
	postgresInfo := libdata.ClusterInfo{
		Driver: postgresDriver,
		URL:    os.Getenv("DATABASE_URL"),
	}
	liborm.Cluster, err = libdata.InitDB(false, &postgresInfo, &postgresInfo, nil)
	if err != nil {
		log.Fatalf("Error initializing db %s", err.Error())
	}
	liborm.Cluster.CacheDriver = &memory.Driver{}
	liborm.Cluster.Register(genmodels.CustomerDefinition.Model)
	liborm.Cluster.Register(genmodels.EventDefinition.Model)
	libdata.EventDriver = postgresDriver

	log.Println("Schema loaded...")

	log.Println("Init done...")
}

func main() {

	cfg := &config.Configuration{
		Sampler: &config.SamplerConfig{
			Type:  "const",
			Param: 1,
		},
		Reporter: &config.ReporterConfig{
			LogSpans:          false,
			CollectorEndpoint: os.Getenv("JAEGER_ENDPOINT"),
		},
	}
	tracer, closer, err := cfg.New("service-customer", config.Logger(jaeger.StdLogger))
	if err != nil {
		panic(fmt.Sprintf("ERROR: cannot init Jaeger: %v\n", err))
	}
	defer closer.Close()
	opentracing.SetGlobalTracer(tracer)

	gengrpc.NewServer(tracer)

	if len(os.Args) > 1 {
		if os.Args[1] == "migrate" {
			err = liborm.Cluster.InitSingleTenant(context.Background())
			if err != nil {
				fmt.Println(errors.Wrap(err, "Couldn't initialize single tenant"))
			}
			return
		}
	}

	port, _ := strconv.Atoi(os.Getenv("CUSTOMER_AUTO_DEPLOY_SERVICE_PORT"))

	log.Println("Listening on port ", port, "...")
	server := libgrpc.Server{}
	log.Fatal(server.Listen(port))

}
