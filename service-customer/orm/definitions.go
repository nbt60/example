package orm

import (
	"gitlab.com/empowerlab/stack/lib-go/libdata"
	"gitlab.com/empowerlab/stack/lib-go/libdata/fields"
	"gitlab.com/empowerlab/stack/lib-go/libdata/specials"
	"gitlab.com/empowerlab/stack/lib-go/liborm"
)

var Definitions *liborm.Definitions

func init() {

	Definitions = &liborm.Definitions{
		Repository: "gitlab.com/empowerlab/example/service-customer",
	}

	Definitions.Register(&liborm.Definition{
		Model: &libdata.ModelDefinition{
			Name: "customer",
			Fields: []libdata.Field{
				&fields.Text{Name: "name", Required: true},
				&fields.Text{Name: "street"},
				&fields.Text{Name: "zip"},
				&fields.Text{Name: "city"},
				&fields.Text{Name: "country"},
			},
			Datetime:    true,
			CanAssignID: false,
		},
	})

	Definitions.Register(&liborm.Definition{
		Model: specials.EventModel,
	})
}
