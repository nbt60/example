package graphql

import (
	"gitlab.com/empowerlab/stack/lib-go/libgraphql"
	// "gitlab.com/empowerlab/stack/lib-go/libgrpc"
	// "gitlab.com/empowerlab/stack/lib-go/libmodel"

	"gitlab.com/empowerlab/example/service-customer/grpc"
)

var Definitions *libgraphql.Definitions

func init() {

	Definitions = &libgraphql.Definitions{
		Name:       "customer",
		Repository: "gitlab.com/empowerlab/example/service-customer",
	}

	Definitions.Register(&libgraphql.Definition{
		Grpc: grpc.Definitions.GetByID("customer"),
	})

}
