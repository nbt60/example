// import React from 'react';
import { AUTH_LOGIN, AUTH_LOGOUT, AUTH_ERROR, AUTH_CHECK } from 'react-admin';
// import OAuth2 from 'simple-oauth2';
import qs from 'query-string';
// import { Redirect } from 'react-router-dom';

import { JSO, Popup } from 'jso'

// import { history } from './App'

// const Hydra = require('ory-hydra-sdk');

// Hydra.ApiClient.instance.basePath = 'http://localhost:3003';
// Hydra.ApiClient.instance.authentications.basic.username = 'b66441cb-5535-4b03-a417-b6628f8d1f19';
// Hydra.ApiClient.instance.authentications.basic.password = 'dho2oo6ScBzG_IC6YlhMLaaL.b';

// const credentials = {
//   client: {
//     id: 'b66441cb-5535-4b03-a417-b6628f8d1f19',
//     secret: 'dho2oo6ScBzG_IC6YlhMLaaL.b',
//   },
//   auth: {
//     tokenHost: 'http://localhost:3003',
//     tokenPath: '/oauth2/token',
//     authorizePath: '/oauth2/auth',
//   },
// };
// const client = oauth2.create(credentials);

const scope = 'openid';

export const clientID = 'test'; //process.env.REACT_APP_HYDRA_CLIENT_ID;
export const clientSecret = 'testtest'; //process.env.REACT_APP_HYDRA_CLIENT_SECRET;

console.log(process.env.REACT_APP_HYDRA_CLUSTER_URL);

let oauth2 = new JSO({
  providerID: "hydra",
  client_id: "test",
  client_secret: clientSecret,
  response_type: 'code',
  redirect_uri: window.location.protocol + '//' + window.location.host + '/login/callback',
  authorization: process.env.REACT_APP_HYDRA_CLUSTER_URL + '/oauth2/auth',
  token: process.env.REACT_APP_HYDRA_CLUSTER_URL + '/oauth2/token',
  scopes: { request: ['offline', 'openid'] },
  debug: true,
})

// export const oauth2 = OAuth2.create({
//   client: {
//     id: clientID,
//     secret: clientSecret,
//   },
//   auth: {
//     tokenHost: process.env.REACT_APP_HYDRA_CLUSTER_URL,
//     authorizePath: '/oauth2/auth',
//     tokenPath: '/oauth2/token',
//   },
//   // These are important for simple-oauth2 to work properly.
//   // options: {
//   //   useBodyAuth: false,
//   //   useBasicAuthorizationHeader: true,
//   // },
// });

// const refreshToken = () => oauth2.clientCredentials
//   .getToken({ scope })
//   .then((result) => {
//     const token = oauth2.accessToken.create(result);
//     // const hydraClient = Hydra.ApiClient.instance;
//     // hydraClient.authentications.oauth2.accessToken = token.token.access_token;
//     console.log(token);
//     return Promise.resolve(token);
//   })

export default (type, params) => {

  console.log(process.env.REACT_APP_DISABLE_AUTH);
  if (process.env.REACT_APP_DISABLE_AUTH === "True") {
    console.log("test")
    return Promise.resolve();
  }

  console.log("type", type);
  console.log("params", params);


  // called when the user attempts to log in
  if (type === AUTH_LOGIN) {
    // refreshToken().then(() => {
    //   console.log('OK');
    // });

    oauth2.getToken({})
      .then((token) => {
        console.log("I got the token: ", token)
      })

    // console.log("login");
    // console.log(oauth2);
    // const authorizationUri = oauth2.authorizationCode.authorizeURL({
    //   redirect_uri: 'http://' + window.location.host + '/login/callback',
    //   scope: 'openid',
    //   state: 'xyzABC123',
    // });
    // console.log(authorizationUri);
    // window.location = authorizationUri;

    // const { username, password } = params;
    // const request = new Request('http://localhost:9000/oauth2/auth', {
    //   method: 'POST',
    //   body: JSON.stringify({ username, password }),
    //   headers: new Headers({ 'Content-Type': 'application/json' }),
    // })
    // return fetch(request)
    //   .then((response) => {
    //     console.log("auth response", response);
    //     if (response.status < 200 || response.status >= 300) {
    //       throw new Error(response.statusText);
    //     }
    //     return response.json();
    //   })
    //   .then(({ token }) => {
    //     localStorage.setItem('token', token);
    //   });
  }
  // called when the user clicks on the logout button
  if (type === AUTH_LOGOUT) {
    localStorage.removeItem('tokens-hydra');
    return Promise.resolve();
  }
  // called when the API returns an error
  if (type === AUTH_ERROR) {
    const { status } = params;
    if (status === 401 || status === 403) {
      localStorage.removeItem('token');
      return Promise.reject();
    }
    return Promise.resolve();
  }
  // called when the user navigates to a new location
  if (type === AUTH_CHECK) {

    if (!localStorage.getItem('tokens-hydra')) {
      console.log("code", qs.parse(window.location.search).code)
      try {

        oauth2.callback({
          'code': qs.parse(window.location.search).code,
          'scope': qs.parse(window.location.search).scope,
          'state': qs.parse(window.location.search).state,
        }).then(() => {
          window.location.replace("/")
        })

        // console.log("orfoorijfoierjfioejr")

        // history.push("/")

        //   oauth2.authorizationCode.getToken({
        //     code: qs.parse(window.location.search).code,
        //     // client_id: clientID,
        //     // client_secret: clientSecret,
        //     redirect_uri: 'http://' + window.location.host + '/login/callback',
        //     scope: 'openid',
        //   }, {}).then((result) => {
        //     // console.log('The resulting token: ', result);

        //     return oauth2.accessToken.create(result)
        //   }).then((token) => {
        //     console.log('token: ', token);
        //     console.log('token: ', token.token);
        //     console.log('token: ', JSON.stringify(token.token));
        //     localStorage.setItem('token', JSON.stringify(token.token));
        //     // props.history.push('/');
        //   }).catch((error) => {
        //     console.error('Access Token Error', error.message);
        //   });

        //   // return res.status(200).json(token)
      } catch (error) {
        console.error('Access Token Error', error.message);
        // return res.status(500).json('Authentication failed');
      }

    }

    return localStorage.getItem('tokens-hydra') ? Promise.resolve() : Promise.reject();
  }
  return Promise.reject('Unknown method');
};
