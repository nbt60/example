import React from 'react';
import PropTypes from 'prop-types';
import { 
  Filter, Responsive, SimpleList, List, Edit, Create, Datagrid, 
  ReferenceField, EditButton, DisabledInput, TextField, LongTextInput, 
  BooleanField, BooleanInput, ReferenceInput, SelectInput, SimpleForm, TextInput,
  NullableBooleanInput, ReferenceManyField, ReferenceArrayInput, SelectArrayInput, NumberInput, NumberField,
} from 'react-admin';
// import ActionButton from './mui/Field/ActionButton';
// import TextField from 'admin-on-rest/src/mui/field/TextField';

// const ShopFilter = props => (
//   <Filter {...props}>
//     <TextInput label="Search" source="q" alwaysOn />
//   </Filter>
// );

const InvoiceFilter = props => (
  <Filter {...props}>
    <TextInput label="ID" source="id" alwaysOn />
  </Filter>
);

export const InvoiceList = props => (
  <List {...props} filters={<InvoiceFilter />}>
    <Datagrid>
      <TextField source="id" />
      <TextField source="number" />
      <TextField source="customerUUID"/>
      <TextField source="state" />
      <EditButton />
    </Datagrid>
  </List>
);

export const InvoiceCreate = props => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="number" />
      <ReferenceInput label="Customer" source="customerUUID" reference="Customer" allowEmpty>
        <SelectInput optionText="name" />
      </ReferenceInput>
      <TextInput source="state" />
      {/* <ReferenceInput label="Source Image" source="sourceImageID" reference="Image" allowEmpty>
        <SelectInput optionText="id" />
      </ReferenceInput> */}
      {/* <ReferenceArrayInput label="Childs" source="childIds" reference="Application" allowEmpty>
        <SelectArrayInput optionText="id" />
      </ReferenceArrayInput> */}
    </SimpleForm>
  </Create>
);

export const InvoiceEdit = props => (
  <Edit {...props}>
    <SimpleForm>
      <DisabledInput source="id" />
      <TextInput source="number" />
      <ReferenceInput label="Customer" source="customerUUID" reference="Customer" allowEmpty>
        <SelectInput optionText="name" />
      </ReferenceInput>
      <TextInput source="state" />
      {/* <ReferenceArrayInput label="Childs" source="childIds" reference="Application" allowEmpty>
        <SelectArrayInput optionText="id" />
      </ReferenceArrayInput>
      <ReferenceManyField
        label="Options"
        reference="ApplicationOption"
        target="applicationID"
        // sort={{ field: 'recordID', order: 'DESC' }}
      >
        <Datagrid>
          <TextField source="key" />
          <TextField source="value" />
          <EditButton />
        </Datagrid>
      </ReferenceManyField>
      <ReferenceManyField
        label="Deployments"
        reference="ApplicationDeployment"
        target="applicationID"
        // sort={{ field: 'recordID', order: 'DESC' }}
      >
        <Datagrid>
          <TextField source="providerID" />
          <TextField source="deployerID" />
          <TextField source="model" />
          <BooleanField source="auto" />
          <NumberField source="sequence" />
          <TextField source="actionDeploy" />
          <TextField source="actionPurge" />
          <EditButton />
        </Datagrid>
      </ReferenceManyField> */}
    </SimpleForm>
  </Edit>
);
