import React from 'react';
import PropTypes from 'prop-types';
import { 
  Filter, Responsive, SimpleList, List, Edit, Create, Datagrid, 
  ReferenceField, EditButton, DisabledInput, TextField, LongTextInput, 
  BooleanField, BooleanInput, ReferenceInput, SelectInput, SimpleForm, TextInput,
  NullableBooleanInput, ReferenceManyField, ReferenceArrayInput, SelectArrayInput, NumberInput, NumberField,
} from 'react-admin';
// import ActionButton from './mui/Field/ActionButton';
// import TextField from 'admin-on-rest/src/mui/field/TextField';

// const ShopFilter = props => (
//   <Filter {...props}>
//     <TextInput label="Search" source="q" alwaysOn />
//   </Filter>
// );

const InvoiceLineFilter = props => (
  <Filter {...props}>
    <TextInput label="ID" source="id" alwaysOn />
  </Filter>
);

export const InvoiceLineList = props => (
  <List {...props} filters={<InvoiceLineFilter />}>
    <Datagrid>
      <TextField source="id" />
      <TextField source="invoiceUUID"/>
      <TextField source="productUUID"/>
      <TextField source="name" />
      <NumberField source="quantity" />
      <NumberField source="priceUnit" />
      <EditButton />
    </Datagrid>
  </List>
);

export const InvoiceLineCreate = props => (
  <Create {...props}>
    <SimpleForm>
      <ReferenceInput label="Invoice"source="invoiceUUID" reference="Invoice" allowEmpty>
        <TextField optionText="id" />
      </ReferenceInput>
      <ReferenceInput label="Product" source="productUUID" reference="Product" allowEmpty>
        <SelectInput optionText="name" />
      </ReferenceInput>
      <TextInput source="name" />
      <NumberInput source="quantity" />
      <NumberInput source="priceUnit" />
    </SimpleForm>
  </Create>
);

export const InvoiceLineEdit = props => (
  <Edit {...props}>
    <SimpleForm>
      <DisabledInput source="id" />
      <ReferenceInput label="Invoice" source="invoiceUUID" reference="Invoice" allowEmpty>
        <TextField optionText="number" />
      </ReferenceInput>
      <ReferenceInput label="Product" source="productUUID" reference="Product" allowEmpty>
        <SelectInput optionText="name" />
      </ReferenceInput>
      <TextInput source="name" />
      <NumberInput source="quantity" />
      <NumberInput source="priceUnit" />
    </SimpleForm>
  </Edit>
);
