import React from 'react';
import PropTypes from 'prop-types';
import { 
  Filter, Responsive, SimpleList, List, Edit, Create, Datagrid, 
  ReferenceField, EditButton, DisabledInput, TextField, LongTextInput, 
  BooleanField, BooleanInput, ReferenceInput, SelectInput, SimpleForm, TextInput, NumberInput,
  NullableBooleanInput, ReferenceManyField,
} from 'react-admin';
// import ActionButton from './mui/Field/ActionButton';
// import TextField from 'admin-on-rest/src/mui/field/TextField';

// const ShopFilter = props => (
//   <Filter {...props}>
//     <TextInput label="Search" source="q" alwaysOn />
//   </Filter>
// );

const RoleFilter = props => (
  <Filter {...props}>
    <TextInput label="ID" source="id" alwaysOn />
  </Filter>
);

export const RoleList = props => (
  <List {...props} filters={<RoleFilter />}>
    <Datagrid>
      <TextField source="id" />
      <TextField source="accountUUID"/>
      <TextField source="name" />
      <EditButton />
    </Datagrid>
  </List>
);

export const RoleCreate = props => (
  <Create {...props}>
    <SimpleForm>
      <ReferenceInput label="Account" source="accountUUID" reference="Account" allowEmpty>
        <SelectInput optionText="name" />
      </ReferenceInput>
      <TextInput source="name" />
    </SimpleForm>
  </Create>
);

export const RoleEdit = props => (
  <Edit {...props}>
    <SimpleForm>
      <DisabledInput source="id" />
      <ReferenceInput label="Account" source="accountUUID" reference="Account" allowEmpty>
        <SelectInput optionText="name" />
      </ReferenceInput>
      <TextInput source="name" />
    </SimpleForm>
  </Edit>
);
