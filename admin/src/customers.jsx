import React from 'react';
import PropTypes from 'prop-types';
import { 
  Filter, Responsive, SimpleList, List, Edit, Create, Datagrid, 
  ReferenceField, EditButton, DisabledInput, TextField, LongTextInput, 
  BooleanField, BooleanInput, ReferenceInput, SelectInput, SimpleForm, TextInput,
  NullableBooleanInput, ReferenceManyField,
} from 'react-admin';
// import ActionButton from './mui/Field/ActionButton';
// import TextField from 'admin-on-rest/src/mui/field/TextField';

// const ShopFilter = props => (
//   <Filter {...props}>
//     <TextInput label="Search" source="q" alwaysOn />
//   </Filter>
// );

const CustomerFilter = props => (
  <Filter {...props}>
    <TextInput label="ID" source="id" alwaysOn />
  </Filter>
);

export const CustomerList = props => (
  <List {...props} filters={<CustomerFilter />}>
    <Datagrid>
      <TextField source="id" />
      <TextField source="name" />
      <EditButton />
    </Datagrid>
  </List>
);

export const CustomerCreate = props => (
  <Create {...props}>
    <SimpleForm>
      <TextInput source="name" />
      <TextInput source="street" />
      <TextInput source="zip" />
      <TextInput source="city" />
      <TextInput source="country" />
    </SimpleForm>
  </Create>
);

export const CustomerEdit = props => (
  <Edit {...props}>
    <SimpleForm>
      <DisabledInput source="id" />
      <TextInput source="name" />
      <TextInput source="street" />
      <TextInput source="zip" />
      <TextInput source="city" />
      <TextInput source="country" />
    </SimpleForm>
  </Edit>
);
