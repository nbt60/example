import React, { Component } from 'react';
import buildOpenCrudProvider, { buildQuery } from 'ra-data-opencrud';
import { Admin, Resource } from 'react-admin';
import { setContext } from 'apollo-link-context';
import { createHttpLink } from 'apollo-link-http';
import { createBrowserHistory as createHistory } from 'history';

import { CustomerCreate, CustomerEdit, CustomerList } from './customers';
import { ProductCreate, ProductEdit, ProductList } from './products';
import { InvoiceCreate, InvoiceEdit, InvoiceList } from './invoices';
import { InvoiceLineCreate, InvoiceLineEdit, InvoiceLineList } from './invoice_lines';
import { AccountCreate, AccountEdit, AccountList } from './accounts';
import { RoleCreate, RoleEdit, RoleList } from './roles';
import authClient from './authClient';
// import customRoutes from './routes';


// export const history = createHistory();

const httpLink = createHttpLink({
  uri: process.env.REACT_APP_GATEWAY_SERVICE_URL,
});


const authLink = setContext((_, { headers }) => {
  // get the authentication token from local storage if it exists
  const token = localStorage.getItem('tokens-hydra');
  // return the headers to the context so httpLink can read them
  console.log('token', token);
  console.log('auth')
  // console.log('token', JSON.parse(token).access_token);
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${JSON.parse(token)[0].access_token}` : '',
    },
  };
});


class App extends Component {
  constructor() {
      super();
      this.state = { dataProvider: null };
  }
  componentDidMount() {

    const enhanceBuildQuery = introspection => (fetchType, resource, params) => {
        const builtQuery = buildQuery(introspection)(fetchType, resource, params);

        // console.log("test")
        console.log(builtQuery)

        return builtQuery;
    }

    console.log(process.env)
    console.log("gateway_url", process.env.REACT_APP_GATEWAY_SERVICE_URL)

    const token = localStorage.getItem('tokens-hydra');

    buildOpenCrudProvider({ clientOptions: {
      // uri: process.env.REACT_APP_GATEWAY_SERVICE_URL,
      link: authLink.concat(httpLink),  
    }, buildQuery: enhanceBuildQuery})
        .then(dataProvider => this.setState({ dataProvider }));
  }

  render() {

    const { dataProvider } = this.state;

    if (!dataProvider) {
        return <div>Loading</div>;
    }

    let runningAuthClient = authClient
    if (process.env.REACT_APP_DISABLE_AUTH === "True") {
      runningAuthClient = false
    }

    console.log("disableAuth", process.env.REACT_APP_DISABLE_AUTH);
    console.log("authClient", runningAuthClient);
    // console.log("customerRoutes", customRoutes);
    // customRoutes={customRoutes}>
    // history={history}>
    return (
      <Admin dataProvider={dataProvider}  authProvider={runningAuthClient}> 
           <Resource name="Customer" list={CustomerList} edit={CustomerEdit} create={CustomerCreate} />
           <Resource name="Product" list={ProductList} edit={ProductEdit} create={ProductCreate} />
           <Resource name="Invoice" list={InvoiceList} edit={InvoiceEdit} create={InvoiceCreate} />
           <Resource name="InvoiceLine" list={InvoiceLineList} edit={InvoiceLineEdit} create={InvoiceLineCreate} />
           <Resource name="Account" list={AccountList} edit={AccountEdit} create={AccountCreate} />
           <Resource name="Role" list={RoleList} edit={RoleEdit} create={RoleCreate} />
      </Admin>
    );
  }
}

export default App;
