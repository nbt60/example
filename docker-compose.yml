version: '3.7'
services:
  nats:
    image: nats:latest
    networks:
      - empower
  jaeger:
    image: 'jaegertracing/all-in-one:1.14.0'
    networks:
      - empower
    ports:
      - "5004:16686"

  hydra-postgresql:
    image: 'postgres:12.0-alpine'
    networks:
      - empower
    environment:
      POSTGRES_USER: empower
      POSTGRES_PASSWORD: empower
      POSTGRES_DB: empower
  hydra-init:
    image: oryd/hydra:v1.0.8-alpine
    networks:
      - empower
    depends_on:
      - hydra
    command: ["migrate", "sql", "-e", "--yes", "--config", "/etc/config/config.yaml"]
    environment:
      DSN: "postgres://empower:empower@hydra-postgresql:5432/empower?sslmode=disable"
      URLS_SELF_ISSUER: "http://localhost:5002"
      URLS_CONSENT: "http://localhost:5003/consent"
      URLS_LOGIN: "http://localhost:5003/login"
      SECRETS_SYSTEM: "this_needs_to_be_the_same_always_and_also_very_cuR3-._"
      SERVE_PUBLIC_CORS_ENABLED: 'true'
      SERVER_PUBLIC_CORS_ALLOWED_ORIGINS: '*'
      SERVER_PUBLIC_CORS_ALLOWED_HEADERS: 'Authorization,Content-Type'
      SERVER_PUBLIC_CORS_ALLOWED_HEADERS: 'POST,GET,PUT,DELETE'
  hydra-init2:
    image: oryd/hydra:v1.0.8-alpine
    networks:
      - empower
    depends_on:
      - hydra
    command: [
      "clients", "create", "--skip-tls-verify", 
      "--id", "test", "--secret", "testtest", 
      "--grant-types", "authorization_code,refresh_token,client_credentials,implicit,introspect",
      "--response-types", "token,code,id_token", "--scope", "openid,offline", 
      "--callbacks", "http://localhost:5000/login/callback"]
    environment:
      HYDRA_ADMIN_URL: "http://hydra:4445"
      DSN: "postgres://empower:empower@hydra-postgresql:5432/empower?sslmode=disable"
      URLS_SELF_ISSUER: "http://localhost:5002"
      URLS_CONSENT: "http://localhost:5003/consent"
      URLS_LOGIN: "http://localhost:5003/login"
      SECRETS_SYSTEM: "this_needs_to_be_the_same_always_and_also_very_cuR3-._"
      SERVE_PUBLIC_CORS_ENABLED: 'true'
      SERVER_PUBLIC_CORS_ALLOWED_ORIGINS: '*'
      SERVER_PUBLIC_CORS_ALLOWED_HEADERS: 'Authorization,Content-Type'
      SERVER_PUBLIC_CORS_ALLOWED_HEADERS: 'POST,GET,PUT,DELETE'
  hydra:
    image: oryd/hydra:v1.0.8-alpine
    networks:
      - empower
    depends_on:
      - hydra-postgresql
    ports:
      - "5002:4444"
    command: [
      "serve",
      "all",
      "--dangerous-force-http",
      "--dangerous-allow-insecure-redirect-urls",
      "http://localhost:5000",
      "--config",
      "/etc/config/config.yaml"
    ]
    environment:
      DSN: "postgres://empower:empower@hydra-postgresql:5432/empower?sslmode=disable"
      URLS_SELF_ISSUER: "http://localhost:5002"
      URLS_CONSENT: "http://localhost:5003/consent"
      URLS_LOGIN: "http://localhost:5003/login"
      SECRETS_SYSTEM: "this_needs_to_be_the_same_always_and_also_very_cuR3-._"
      SERVE_PUBLIC_CORS_ENABLED: 'true'
      SERVER_PUBLIC_CORS_ALLOWED_ORIGINS: '*'
      SERVER_PUBLIC_CORS_ALLOWED_HEADERS: 'Authorization,Content-Type'
      SERVER_PUBLIC_CORS_ALLOWED_HEADERS: 'POST,GET,PUT,DELETE'

  postgresql:
    image: 'postgres:12.0-alpine'
    networks:
      - empower
    environment:
      POSTGRES_USER: empower
      POSTGRES_PASSWORD: empower
      POSTGRES_DB: empower

  login-init:
    image: registry.gitlab.com/empowerlab/stack/login/master:latest
    networks:
      - empower
    depends_on:
      - hydra
      - postgresql
      - nats
      - jaeger
    working_dir: /go/src/gitlab.com/empowerlab/stack/login
    command: ["login", "migrate"]
    environment:
      DATABASE_URL: "postgres://empower:empower@postgresql:5432/empower?sslmode=disable"
      LOGIN_AUTO_DEPLOY_SERVICE_PORT: "5000"
      PORT_GRPC: "5001"
      NATS_HOST: "nats://nats:4222"
      JAEGER_ENDPOINT: "http://jaeger:14268/api/traces"
      HYDRA_CLIENT_ID: "test"
      HYDRA_CLIENT_SECRET: "testtest"
      HYDRA_CLUSTER_URL: "http://hydra:4445"
      LOGIN_CLUSTER_URL: "http://login"
    # Uncomment to link to your local dev environment
    # volumes:
    #   - "$GOPATH:/go"
  login:
    image: registry.gitlab.com/empowerlab/stack/login/master:latest
    networks:
      - empower
    depends_on:
      - hydra
      - postgresql
      - nats
      - jaeger
    ports:
      - "5003:5003"
    working_dir: /go/src/gitlab.com/empowerlab/stack/login
    command: "login"
    environment:
      DATABASE_URL: "postgres://empower:empower@postgresql:5432/empower?sslmode=disable"
      LOGIN_AUTO_DEPLOY_SERVICE_PORT: "5003"
      PORT_GRPC: "5001"
      NATS_HOST: "nats://nats:4222"
      JAEGER_ENDPOINT: "http://jaeger:14268/api/traces"
      HYDRA_CLIENT_ID: "test"
      HYDRA_CLIENT_SECRET: "testtest"
      HYDRA_CLUSTER_URL: "http://hydra:4445"
      LOGIN_CLUSTER_URL: "http://localhost:5002"
    # Uncomment to link to your local dev environment
    # volumes:
    #   - "$GOPATH:/go"

  customer-init:
    image: registry.gitlab.com/empowerlab/example/go-factory/master:latest
    networks:
      - empower
    depends_on:
      - postgresql
      - nats
      - jaeger
    working_dir: /go/src/gitlab.com/empowerlab/example/service-customer
    command: ["service-customer", "migrate"]
    environment:
      DATABASE_URL: "postgres://empower:empower@postgresql:5432/empower?sslmode=disable"
      CUSTOMER_AUTO_DEPLOY_SERVICE_PORT: "5000"
      NATS_HOST: "nats://nats:4222"
      JAEGER_ENDPOINT: "http://jaeger:14268/api/traces"
    # Uncomment to link to your local dev environment
    # volumes:
    #   - "$GOPATH:/go"
  customer:
    image: registry.gitlab.com/empowerlab/example/go-factory/master:latest
    networks:
      - empower
    depends_on:
      - postgresql
      - nats
      - jaeger
    working_dir: /go/src/gitlab.com/empowerlab/example/service-customer
    command: "service-customer"
    environment:
      DATABASE_URL: "postgres://empower:empower@postgresql:5432/empower?sslmode=disable"
      CUSTOMER_AUTO_DEPLOY_SERVICE_PORT: "5000"
      NATS_HOST: "nats://nats:4222"
      JAEGER_ENDPOINT: "http://jaeger:14268/api/traces"
    # Uncomment to link to your local dev environment
    # volumes:
    #   - "$GOPATH:/go"


  product-init:
    image: registry.gitlab.com/empowerlab/example/go-factory/master:latest
    networks:
      - empower
    depends_on:
      - postgresql
      - nats
      - jaeger
    working_dir: /go/src/gitlab.com/empowerlab/example/service-product
    command: ["service-product", "migrate"]
    environment:
      DATABASE_URL: "postgres://empower:empower@postgresql:5432/empower?sslmode=disable"
      PRODUCT_AUTO_DEPLOY_SERVICE_PORT: "5000"
      NATS_HOST: "nats://nats:4222"
      JAEGER_ENDPOINT: "http://jaeger:14268/api/traces"
    # Uncomment to link to your local dev environment
    # volumes:
    #   - "$GOPATH:/go"
  product:
    image: registry.gitlab.com/empowerlab/example/go-factory/master:latest
    networks:
      - empower
    depends_on:
      - postgresql
      - nats
      - jaeger
    working_dir: /go/src/gitlab.com/empowerlab/example/service-product
    command: "service-product"
    environment:
      DATABASE_URL: "postgres://empower:empower@postgresql:5432/empower?sslmode=disable"
      PRODUCT_AUTO_DEPLOY_SERVICE_PORT: "5000"
      NATS_HOST: "nats://nats:4222"
      JAEGER_ENDPOINT: "http://jaeger:14268/api/traces"
    # Uncomment to link to your local dev environment
    # volumes:
    #   - "$GOPATH:/go"

  invoicing-init:
    image: registry.gitlab.com/empowerlab/example/go-factory/master:latest
    networks:
      - empower
    depends_on:
      - postgresql
      - nats
      - jaeger
      - customer
      - product
    working_dir: /go/src/gitlab.com/empowerlab/example/service-invoicing
    command: ["service-invoicing", "migrate"]
    environment:
      DATABASE_URL: "postgres://empower:empower@postgresql:5432/empower?sslmode=disable"
      INVOICING_AUTO_DEPLOY_SERVICE_PORT: "5000"
      NATS_HOST: "nats://nats:4222"
      JAEGER_ENDPOINT: "http://jaeger:14268/api/traces"
      CUSTOMER_SERVICE_HOST: "customer:5000"
      PRODUCT_SERVICE_HOST: "product:5000"
    # Uncomment to link to your local dev environment
    # volumes:
    #   - "$GOPATH:/go"      
  invoicing:
    image: registry.gitlab.com/empowerlab/example/go-factory/master:latest
    networks:
      - empower
    depends_on:
      - postgresql
      - nats
      - jaeger
      - customer
      - product
    working_dir: /go/src/gitlab.com/empowerlab/example/service-invoicing
    command: "service-invoicing"
    environment:
      DATABASE_URL: "postgres://empower:empower@postgresql:5432/empower?sslmode=disable"
      INVOICING_AUTO_DEPLOY_SERVICE_PORT: "5000"
      NATS_HOST: "nats://nats:4222"
      JAEGER_ENDPOINT: "http://jaeger:14268/api/traces"
      CUSTOMER_SERVICE_HOST: "customer:5000"
      PRODUCT_SERVICE_HOST: "product:5000"
    # Uncomment to link to your local dev environment
    # volumes:
    # - "$GOPATH:/go"

  gateway:
    image: registry.gitlab.com/empowerlab/example/go-factory/master:latest
    networks:
      - empower
    depends_on:
      - postgresql
      - nats
      - jaeger
      - login
      - customer
      - product
      - invoicing
    ports:
      - "5001:5001"
    working_dir: /go/src/gitlab.com/empowerlab/example/gateway
    command: "gateway"
    environment:
      GATEWAY_AUTO_DEPLOY_SERVICE_PORT: "5001"
      NATS_HOST: "nats://nats:4222"
      JAEGER_ENDPOINT: "http://jaeger:14268/api/traces"
      CUSTOMER_SERVICE_HOST: "customer:5000"
      PRODUCT_SERVICE_HOST: "product:5000"
      INVOICING_SERVICE_HOST: "invoicing:5000"
      LOGIN_SERVICE_HOST: "login:5001"
      HYDRA_CLUSTER_URL: "http://hydra:4445"
    # Uncomment to link to your local dev environment
    # volumes:
    #   - "$GOPATH:/go"

  admin:
    image: registry.gitlab.com/empowerlab/example/admin/master:latest
    networks:
      - empower
    depends_on:
      - hydra
      - gateway
    ports:
      - "5000:5000"
    
    environment:
      ADMIN_AUTO_DEPLOY_SERVICE_PORT: "5000"
      REACT_APP_GATEWAY_SERVICE_URL: "http://localhost:5001/graphql"
      REACT_APP_HYDRA_CLUSTER_URL: "http://localhost:5002"
    # Uncomment to link to your local dev environment
    # command: ["yarn", "run", "start"]
    # volumes:
    #   - "$GOPATH/src/gitlab.com/empowerlab/example/admin:/opt/app-root"

networks:
  empower:
