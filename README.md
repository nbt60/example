# The Empower Stack Example

This project is the exemplary project of the Empower Stack. It is designed to be used as a starting point for your own microservice architecture.

Check the documentation to understand more about what is contained in this project: https://empower.sh/docs/example.

You can use the docker-compose.yml to quickly set up a working installation on your local environment: https://empower.sh/docs/local.

The gitlab-ci.yml contains instructions to build the images and deploy them on a Kubernetes cluster. See this page for more instructions to set up your Kubernetes cluster: https://empower.sh/docs/kubernetes.

The Empower Stack is a set of libraries and exemplary code to easily set up your microservice architecture, following state of the art patterns. Please check https://empower.sh to know more about the project.
